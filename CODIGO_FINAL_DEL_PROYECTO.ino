int conta = 1;
int sensorpir = 7;
int ledV = 8;
int ledR = 13;
unsigned long start = millis()+60000UL;

void setup() 
{
  pinMode (sensorpir,INPUT);
  pinMode (ledV, OUTPUT);
  pinMode (ledR, OUTPUT);
  Serial.begin(9600);
  Serial.println("Empieza el conteo"); 
}
 
void loop()
{
 
  while (millis() < start) 
 {
  
    if(digitalRead(sensorpir) == HIGH)
  {
    Serial.println(conta);
    conta++;
    digitalWrite(ledV, HIGH);
    delay(1400);
    digitalWrite(ledV,LOW);
    
    if(millis() >= 60000UL)
   {
     digitalWrite(ledR, HIGH);
     Serial.println("Termina el conteo");
     Serial.print("El numero de piezas que pasan en un minuto es igual a: " ) + Serial.println(conta-1);
   }
  }
    break;
 } 
} 
